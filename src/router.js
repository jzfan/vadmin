import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Base from './views/Base.vue'
import Dashboard from './views/Dashboard.vue'
import Videos from './views/Videos.vue'
import VideosCreate from './views/VideosCreate.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  linkActiveClass: 'is-active',
  routes: [
    {
      path: '/',
      redirect: '/admin'
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/admin',
      component: Base,
      children: [
        { path: '', component: Dashboard },
        { path: 'videos', component: Videos },
        { path: 'videos/create', component: VideosCreate },

      ]
    }
  ]
})
