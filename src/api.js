import axios from 'axios'
// import store from './store'
import env from '../.env.js'

// axios.defaults.baseURL = env.DEV_API_HOST
axios.defaults.baseURL = process.env.NODE_ENV === 'development' ? env.DEV_API_HOST : env.PRO_API_HOST

function uploadFile(file, cb) {
    let form = new FormData();
    let host = process.env.NODE_ENV === 'development' ? env.DEV_UPLOAD_HOST : env.PRO_UPLOAD_HOST
    form.append("file", file);
    axios
        .post(
            `${host}/storage/uploadFile?appName=mofo&keepFile=0`,
            form
        )
        .then(res => cb(res));
}

function submitVideo(video, cb) {
    axios
        .post("/boss/detail", video)
        .then(() => cb());
}

function login(pass, cb) {
    axios.post("/boss/login", pass)
        .then(res => cb(res))
}

function videoPage(page, pageSize, cb) {
    axios.get(`/boss/queryVideoList?currentPage=${page}&pageSize=${pageSize}`)
        .then((res) => cb(res))
}
export {
    uploadFile,
    submitVideo,
    login,
    videoPage
}