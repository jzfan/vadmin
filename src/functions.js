import bus from './bus'
function setCookie(key, value, days) {
    // 设置过期原则
    if (!value) {
        localStorage.removeItem(key)
    } else {
        var Days = days || 7; // 默认保留7天
        var exp = new Date();
        localStorage[key] = JSON.stringify({
            value,
            expires: exp.getTime() + Days * 24 * 60 * 60 * 1000
        })
    }
}
function getCookie(name) {
    try {
        let o = JSON.parse(localStorage[name])
        if (!o || o.expires < Date.now()) {
            return null
        } else {
            return o.value
        }
    } catch (e) {
        return localStorage[name]
    }
}
function flash(msg) {
    bus.$emit('notice', msg)
}
export {
    setCookie,
    getCookie,
    flash
}